# app/__init__.py

from flask_restplus import Api
from flask import Blueprint

from .main.controller.request_controller import api as request_ns
from .main.controller.stat_controller import api as stat_ns
from .main.controller.pincode_controller import api as pincode_ns

blueprint = Blueprint('api', __name__, url_prefix='/api')

api = Api(blueprint,
            title = 'Flask RESTplus api for GetCovaxIN',
            version = '1.0',
            description = 'api for flask restplus web service and MQTT pub server',
            doc='/docs'
        )

api.add_namespace(request_ns, path='/requests')
api.add_namespace(stat_ns, path='/stats')
api.add_namespace(pincode_ns, path='/pincodes')
from flask import request
from flask_restplus import Resource
from flask import app
from flask import current_app as app_config

from ..util.dto import statDto
from ..service.stats_service import get_requests_stats, get_pincode_stats
from ..util.error_responses import *


api = statDto.api
_passcode = statDto.passcode

# ----- Get Requests Stats ---------- #
@api.route('/requests', methods=['POST'])
class getStats(Resource):
    # define responses
    @api.response(200, "Request Stats fetched")
    @api.response(403, "Resource error")
    @api.response(406, "Validation error")

    @api.doc("Get stats for requests table")
    @api.expect(_passcode, validate=True)
    def post(self):
        """Request table stats"""
        passcode = request.json['passcode']
        if not passcode == app_config.config['STAT_PASSCODE']:
            return error_response_401("Unauthorized error")

        return get_requests_stats()
    
# ----- Get Pincode Stats ---------- #
@api.route('/pincodes', methods=['POST'])
class getStats(Resource):
    # define responses
    @api.response(200, "Pincode Stats fetched")
    @api.response(403, "Resource error")
    @api.response(406, "Validation error")

    @api.doc("Get stats for pincodes table")
    @api.expect(_passcode, validate=True)
    def post(self):
        """Pincode table stats"""
        passcode = request.json['passcode']
        if not passcode == app_config.config['STAT_PASSCODE']:
            return error_response_401("Unauthorized error")

        return get_pincode_stats()
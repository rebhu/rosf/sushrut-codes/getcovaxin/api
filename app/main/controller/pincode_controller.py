from flask import request, make_response
from flask_restplus import Resource
from flask import app
import json

from ..util.dto import pincodeDto
from ..service.pincode_service import register_pincode_slots
from ..util.error_responses import *

api = pincodeDto.api

# --------- REGISTER PINCODE SLOTS -----------------
@api.route('/<pincode>/<payload_in>', methods=['PUT'])
@api.doc(params={'pincode': 'Pincode'}, validate=True)
@api.doc(params={'payload_in': 'Payload'}, validate=True)
class registerSlots(Resource):
    # define responses
    @api.response(201, "Slot registered")
    @api.response(403, "Resource error")
    @api.response(406, "Validation error")

    @api.doc("Slot registration")
    # @api.expect(_slots, validate=True)
    def put(self, pincode, payload_in):
        """Register pincode slots"""
        
        payload = json.loads(payload_in)
        payload['pincode'] = pincode
        return register_pincode_slots(data=payload)

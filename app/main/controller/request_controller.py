from flask import request, make_response
from flask_restplus import Resource
from flask import app
import datetime as dt
from flask import current_app as app_config

from ..util.dto import requestsDto
from ..service.request_service import register_pincode, store_sub_token, push_notification, update_n_push_count, request_id_verification
from ..service.slot_pub_service import push_slots
from ..util.error_responses import *

api = requestsDto.api
_request = requestsDto.request
_request_id = requestsDto.request_id
_sub_token = requestsDto.sub_token
_push_notification = requestsDto.push_notification



# ----- REGISTER PINCODE ---------- #
@api.route('', methods=['POST'])
class registerPincode(Resource):
    # define responses
    @api.response(201, "Pincode registered")
    @api.response(403, "Resource error")
    @api.response(406, "Validation error")

    @api.doc("Pincode registration")
    @api.expect(_request, validate=True)
    def post(self):
        """Register pincode"""
        
        payload = request.json
        ip_addr = request.headers['X-Forwarded-For']
        # ip_addr = request.remote_addr

        resp, code = register_pincode(data=payload, user_agent={"os": request.user_agent.platform, "browser": request.user_agent.browser, "ip_addr": ip_addr})
        
        response = make_response(resp, code)
        if code == 201:
            response.set_cookie(key='_request_id', value=resp['request_id'], httponly=True, secure=True, expires=dt.datetime.utcnow() + dt.timedelta(days=365))
        return response

# ----- VERIFY REQUEST ID ---------- #
# @api.route('/verify', methods=['POST'])
# class verifyRequest(Resource):
#     # define responses
#     @api.response(200, "Request ID verified")
#     @api.response(403, "Resource error")
#     @api.response(406, "Validation error")

#     @api.doc("Request verification")
#     @api.expect(_request_id, validate=True)
#     def post(self):
#         """Request verification"""
        
#         payload = request.json
#         return request_id_verification(data=payload)


# ----- PUSH SUBSCRIPTION ----------- #
@api.route('/subscription', methods=['GET', 'POST', 'PUT'])
class requestSubscription(Resource):
    # define responses            [GET]
    @api.response(200, "Token fetched")
    @api.response(403, "Resource error")
    @api.response(406, "Validation error")

    @api.doc("Fetch token")
    def get(self):
        """Fetch Token"""
        if not verify_user(request):
            return error_response_403("user not verified")
        
        response_object = {
            "sub_token": app_config.config['VAPID_PUBLIC_KEY']
        }
        return response_object, 200

    # define responses            [POST]
    @api.response(200, "Sub token stored")
    @api.response(403, "Resource error")
    @api.response(406, "Validation error")

    @api.doc("Post token")
    @api.expect(_sub_token, validate=True)
    def post(self):
        """Post sub token"""
        if not verify_user(request):
            return error_response_403("user not verified")
        
        payload = request.json
        
        return store_sub_token(data=payload)
    

# --------- ADD PUSH RECEIVED NUMBER ------------
@api.route('/<request_id>', methods=['PUT'])
@api.doc(params={'request_id': 'Request ID'}, validate=True)
class updatePush(Resource):
    # define responses            [PUT]
    @api.response(200, "Number of push updated")
    @api.response(403, "Resource error")
    @api.response(406, "Validation error")

    @api.doc("Update push numbers")
    def put(self, request_id):
        """Update number of push"""
        payload = {
            'request_id': request_id
        }
        return update_n_push_count(data=payload)
    

# ----- FORCED PUSH SUBSCRIPTION ----------- #
@api.route('/push', methods=['POST'])
class pushNotification(Resource):
    # define responses            [POST]
    @api.response(200, "Notification pushed")
    @api.response(403, "Resource error")
    @api.response(406, "Validation error")

    @api.doc("Push Notification")
    @api.expect(_push_notification, validate=True)
    def post(self):
        """Push notification"""
        if not verify_user(request):
            return error_response_403("user not verified")
        
        payload = request.json

        return push_notification(data=payload)



# # ----------- TESTING
# @api.route('/test', methods=['GET'])
# class testApi(Resource):
#     def get(self):
#         # update_slots()
#         push_slots()
#         return 'ss', 200

# ----- Support functions

def verify_user(request):
    if not 'Cookie' in request.headers:
        return False
        
    if not '_request_id' in request.cookies:
        return False

    request_id = request.cookies['_request_id']

    resp, code = request_id_verification(data={'request_id': request_id})
    if code == 200:
        return True
    else:
        False
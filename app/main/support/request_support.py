import re
import ipinfo

from flask import current_app as app
from app.main import db
from ..model.getcovaxin_model import Requests, Pincodes


def get_lat_lon_from_ip(ip_addr):
    # ip_addr = "103.208.69.144"
    handler = ipinfo.getHandler(app.config['IP_TOKEN'])
    details = handler.getDetails(ip_addr)
    
    return float(details.latitude), float(details.longitude)
    

def is_pincode_registered(pincode):
    try:
        pincode_resp = Pincodes.query.filter_by(pincode=pincode).first()

    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]

        return False, message
    
    if pincode_resp:
        return True, 'slot_status'

    else:
        return False, ''


def update_pincode_status(pincode, dict_status):
    try:
        update_resp = Pincodes.query.filter_by(pincode=pincode).update(dict_status)
        db.session.commit()

    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]

        return False, message
    return True, 'Updated'

def verify_request_id(request_id):
    try:
        request_resp = Requests.query.filter_by(request_id=db.func.UUID_TO_BIN(request_id)).first()
        
    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]
        return False, message

    if not request_resp:
        return True, {'success': False}
    else:
        return True, {'success': True}

def number_of_pincodes():
    try:
        num_resp = Pincodes.query.count()
    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]
        return False, message
    return True, num_resp

def update_n_push_received(request_id):
    try:
        n_push_received = Requests.query.filter_by(request_id=db.func.UUID_TO_BIN(request_id)).with_entities(Requests.n_push_received).first()[0]
        n_push_received += 1
        n_push = Requests.query.filter_by(request_id=db.func.UUID_TO_BIN(request_id)).update({'n_push_received': n_push_received}, synchronize_session=False)
        db.session.commit()
    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]
        return False, message
    return True, "Count updated"

def update_n_push_send(request_id):
    try:
        n_push_send = Requests.query.filter_by(request_id=db.func.UUID_TO_BIN(request_id)).with_entities(Requests.n_push_send).first()[0]
        n_push_send += 1
        n_push = Requests.query.filter_by(request_id=db.func.UUID_TO_BIN(request_id)).update({'n_push_send': n_push_send}, synchronize_session=False)
        db.session.commit()
    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]
        return False, message
    return True, "Count updated"

def get_pincode_age(request_id):
    try:
        pincode_age_resp = Requests.query.filter_by(request_id=db.func.UUID_TO_BIN(request_id)).with_entities(Requests.pincode, Requests.age_group).first()
        if not pincode_age_resp:
            return False, 'No request exists'

    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]
        return False, message

    return True, pincode_age_resp[0], pincode_age_resp[1]

# support functions
def B_U(binary_uuid):
    return uuid.UUID(bytes=binary_uuid)

from marshmallow import Schema, fields
from marshmallow.validate import Range, ValidationError, OneOf, Length
from flask import escape

# schema for form data validation
class valSchema(Schema):
    # cylinder
    pincode = fields.Integer(required=True, validate=Range(min=1,max=999999))
    age_group = fields.Integer(required=True, validate=OneOf([18, 45]))
    request_id = fields.Str(required=True, validate=Length(equal=36))
    


# validator
def validate_data(data, validatorList):
    # validate data
    try:
        ValidatorSchema = valSchema(only=validatorList)
    except ValueError:
        return False, 'Invalid field'
    try:
        ValidatorSchema.load(data)
        return True, {}
    
    except ValidationError as err:
        errorType = list(err.messages.keys())[0]
        errorMsg = errorType + ', ' + err.messages[errorType][0]

        return False, errorMsg

def escape_char(data_dict):
    """Function to escape special characters"""
    return {key: str(escape(data_dict[key])) for key in data_dict.keys()}
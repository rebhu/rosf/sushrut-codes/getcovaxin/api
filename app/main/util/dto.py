from flask_restplus import Namespace, fields

class requestsDto:
    api = Namespace('request', description='Pincode register operations')

    # Pincode
    request = api.model('request', {
        'pincode': fields.Integer(required=True, description="Pincode for the request"),
        'age_group': fields.Integer(required=True, description="Age group of the request")     
    })

    # Request ID
    request_id = api.model('request_id', {
        'request_id': fields.String(required=True, description="Request ID"),
    })  

    # Sub Token
    sub_token = api.model('sub_token', {
        'sub_token': fields.String(required=True, description="Sub Token"),
        'request_id': fields.String(required=True, description="request id")
    })  
    
    # Push notification
    push_notification = api.model('push_notification', {
        'sub_token': fields.String(required=True, description="Sub Token"),
        'request_id': fields.String(required=True, description="request id"),
        'text': fields.String(required=True, description="Text")
    })  
    
class statDto:
    api = Namespace('stats', description="Stats for getcovaxin")

    # passcode
    passcode = api.model('passcode', {
        'passcode': fields.String(required=True, description="Passcode to access stats")
    })

class pincodeDto:
    api = Namespace('pincodes', description="Pincodes for requests")

    # slots = api.model('pincode_slots', {
    #     'slots': fields.Nested(
    #         api.model('slots', {
    #             'slot18': fields.Nested(
    #                 api.model('slots18', {
    #                     'dose1': fields.Integer(required=True, description="Slot 18+ dose 1"),
    #                     'dose2': fields.Integer(required=True, description="Slot 18+ dose 1")
    #                 })
    #             ),
    #             'slot45': fields.Nested(
    #                 api.model('slots45', {
    #                     'dose1': fields.Integer(required=True, description="Slot 45+ dose 1"),
    #                     'dose2': fields.Integer(required=True, description="Slot 45+ dose 1")
    #                 })
    #             )
    #         }),
            
    #     ),
    #     'center': fields.Nested(
    #         api.model('center', {
    #             'c18': fields.Integer(required=True, description="Slot 18+ centers"),
    #             'c45': fields.Integer(required=True, description="Slot 45+ centers")
    #         })
    #     ),
    #     'days': fields.Nested(
    #         api.model('days', {
    #             'days18': fields.Integer(required=True, description="Slot 18+ days"),
    #             'days45': fields.Integer(required=True, description="Slot 45+ days")
    #         })
    #     ),
    #     'pincode': fields.Integer(required=True, description="Pincode")
    # })
    

aiohttp==3.7.4.post0
alembic==1.6.2
aniso8601==9.0.1
APScheduler==3.7.0
async-timeout==3.0.1
attrs==21.2.0
cachetools==4.2.2
certifi==2020.12.5
cffi==1.14.5
chardet==4.0.0
click==8.0.0
cryptography==3.4.7
Flask==1.1.2
Flask-APScheduler==1.12.2
Flask-Migrate==2.7.0
flask-restplus==0.13.0
Flask-Script==2.0.6
Flask-SQLAlchemy==2.5.1
greenlet==1.1.0
http-ece==1.1.0
idna==2.10
ipinfo==4.2.0
itsdangerous==2.0.0
Jinja2==3.0.0
jsonschema==3.2.0
Mako==1.1.4
MarkupSafe==2.0.0
marshmallow==3.12.1
multidict==5.1.0
py-vapid==1.8.2
pycparser==2.20
PyMySQL==1.0.2
pyrsistent==0.17.3
python-dateutil==2.8.1
python-editor==1.0.4
pytz==2021.1
pywebpush==1.13.0
requests==2.25.1
six==1.16.0
SQLAlchemy==1.3.19
typing-extensions==3.10.0.0
tzlocal==2.1
urllib3==1.26.4
Werkzeug==2.0.0
yarl==1.6.3
